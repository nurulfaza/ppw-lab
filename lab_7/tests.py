from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, friend_list, add_friend, delete_friend, validate_npm, model_to_dict, get_friend_list
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from unittest.mock import patch
from django.db.models.manager import Manager
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab_7_using_lab_7_template(self):
        response = Client().get('/lab-7/')
        self.assertTemplateUsed(response, 'lab_7/lab_7.html')

    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)
    
    def test_using_daftar_teman_template_for_friend_list(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertTemplateUsed(response, 'lab_7/daftar_teman.html')
    
    def test_lab_7_using_friend_list_func(self):
        found = resolve('/lab-7/friend-list/')
        self.assertEqual(found.func, friend_list)

    def test_lab_7_using_get_friend_list_func(self):
        found = resolve('/lab-7/get-friend-list/')
        self.assertEqual(found.func, get_friend_list)
    
    def test_lab_7_using_add_friend_func(self):
        found = resolve('/lab-7/add-friend/')
        self.assertEqual(found.func, add_friend)

    def test_lab_7_post_success(self):
        response = Client().post('/lab-7/add-friend/', {'name': 'Faza', 'npm': '16068'})
        friends_count = Friend.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(friends_count,1)
    
    def test_get_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="Faza", npm="160688")
        response = Client().post('/lab-7/delete-friend/' + str(friend.id) + '/')
        friends = Friend.objects.all()
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, friends)

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken':False})

    def test_csui_helper_wrong_password(self):
        with self.assertRaises(Exception):
            csui_helper2 = CSUIhelper(username="wrong", password="salah")

    def test_csui_helper(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
