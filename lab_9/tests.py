from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .api_enterkomputer import get_drones, get_soundcards, get_opticals
from .csui_helper import get_access_token, verify_user, get_client_id, get_data_user
import requests
import environ
from django.urls import reverse
from .custom_auth import auth_login, auth_logout
# Create your tests here.

root = environ.Path(__file__) - 3 #three folder back
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"

class Lab9UnitTest(TestCase):
	def test_url_lab9_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_using_lab_9_template(self):
		#ketika belum login
		response = Client().get('/lab-9/')
		self.assertTemplateUsed(response, 'lab_9/session/login.html')
		#ketika login
		session = self.client.session
		session['user_login'] = 'faza'
		session['kode_identitas'] = '0812'
		session.save()
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code, 302)

	def test_lab9_using_index_func(self):
		found = resolve('/lab-9/')
		self.assertEqual(found.func, index)

	def test_get_drones_func(self):
		DRONE_API = 'https://www.enterkomputer.com/api/product/drone.json'
		drones = requests.get(DRONE_API)
		self.assertEqual(drones.json(), get_drones().json())
	
	def test_get_soundcards_func(self):
		SOUNDCARD_API = 'https://www.enterkomputer.com/api/product/soundcard.json'
		soundcards = requests.get(SOUNDCARD_API)
		self.assertEqual(soundcards.json(), get_soundcards().json())

	def test_get_opticals_func(self):
		OPTICAL_API = 'https://www.enterkomputer.com/api/product/optical.json'
		opticals = requests.get(OPTICAL_API)
		self.assertEqual(opticals.json(), get_opticals().json())

	def test_profile(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('nurul.faza', html_response)

	def test_profile_not_login(self):
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 302)

	def test_add_delete_and_reset_fav_drone(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})        

		response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107894}))
		response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107893}))
		response = self.client.post(reverse('lab-9:profile'))

		response_post = self.client.post(reverse('lab-9:del_session_drones', kwargs={'id':107894}))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Berhasil hapus dari favorite',html_response)

		response_post = self.client.post(reverse('lab-9:clear_session_drones'))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Berhasil reset favorite drones',html_response)

		response_post = self.client.post(reverse('lab-9:auth_logout'))


	def test_add_delete_and_reset_fav_optical_and_soundcards(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})        

		response_post = self.client.post(reverse('lab-9:add_session_soundcards', kwargs={'id':53496}))
		response_post = self.client.post(reverse('lab-9:add_session_soundcards', kwargs={'id':53495}))
		response = self.client.post(reverse('lab-9:profile'))

		response_post = self.client.post(reverse('lab-9:add_session_opticals', kwargs={'id':4459}))
		response_post = self.client.post(reverse('lab-9:add_session_opticals', kwargs={'id':4458}))
		response = self.client.post(reverse('lab-9:profile'))

		response_post = self.client.post(reverse('lab-9:del_session_soundcards', kwargs={'id':53496}))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Berhasil hapus dari favorite',html_response)

		response_post = self.client.post(reverse('lab-9:del_session_opticals', kwargs={'id':4459}))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Berhasil hapus dari favorite',html_response)		

		response_post = self.client.post(reverse('lab-9:clear_session_soundcards'))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Berhasil reset favorite soundcards',html_response)

		response_post = self.client.post(reverse('lab-9:clear_session_opticals'))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Berhasil reset favorite opticals',html_response)

		response_post = self.client.post(reverse('lab-9:clear_session_soundcards'))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Tidak ada favorite soundcards',html_response)

		response_post = self.client.post(reverse('lab-9:clear_session_opticals'))
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('Tidak ada favorite opticals',html_response)

		response_post = self.client.post(reverse('lab-9:auth_logout'))


	def test_login_auth(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
		self.assertEqual(response_post.status_code, 302)

	def test_fail_login(self):
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': 'test', 'password': 'pass'})
		response = self.client.get('/lab-9/')
		html_response = response.content.decode('utf8')
		self.assertIn('Username atau password salah', html_response)

	def test_logout_auth(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
		response = self.client.post(reverse('lab-9:auth_logout'))
		response = self.client.get('/lab-9/')
		html_response = response.content.decode('utf8')
		self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus', html_response)

	def test_username_and_pass_wrong(self):
		self.username = "test"
		self.password = "pass"
		access_token = get_access_token(self.username, self.password)
		self.assertEqual(access_token, None)

	def test_verify_func(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		access_token = get_access_token(self.username, self.password)
		parameters = {"access_token": access_token, "client_id":get_client_id()}
		response = requests.get(API_VERIFY_USER, params=parameters)
		result = verify_user(access_token)
		self.assertEqual(result, response.json())

	def test_get_data_user_func(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		self.npm = "1606886186"
		access_token = get_access_token(self.username, self.password)
		parameters = {"access_token":access_token, "client_id":get_client_id()}
		response = requests.get(API_MAHASISWA+self.npm, params=parameters)
		result = get_data_user(access_token, self.npm)
		self.assertEqual(result, response.json())

	def test_login_cookie_page(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
		
		response_post = self.client.get(reverse('lab-9:cookie_login'))
		self.assertTemplateUsed(response_post, 'lab_9/cookie/login.html')
		
		response_post = self.client.get(reverse('lab-9:cookie_auth_login'))
		self.assertEqual(response_post.status_code, 302)

		response_post = self.client.get(reverse('lab-9:cookie_profile'))
		self.assertEqual(response_post.status_code, 302)

		response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username':'test', 'password':'haha'})
		response_post = self.client.get(reverse('lab-9:cookie_login'))
		html_response = response_post.content.decode('utf8')
		self.assertIn('Username atau Password Salah', html_response)

		response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username':'redvelvet', 'password':'tiramisu'})
		response_post = self.client.get(reverse('lab-9:cookie_login'))
		response_post = self.client.get(reverse('lab-9:cookie_profile'))
		self.assertTemplateUsed(response_post, 'lab_9/cookie/profile.html')

		response = self.client.get(reverse('lab-9:cookie_profile'))
		response.client.cookies['user_login'] = 'abcsd'
		response_post = self.client.get(reverse('lab-9:cookie_profile'))
		self.assertTemplateUsed(response_post, 'lab_9/cookie/login.html')

		response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username': 'redvelvet', 'password': 'tiramisu'})
		response_post = self.client.get(reverse('lab-9:cookie_clear'))
		self.assertEqual(response_post.status_code, 302)
