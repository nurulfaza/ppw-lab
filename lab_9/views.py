# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
#catatan: tidak bisa menampilkan messages jika bukan menggunakan method 'render'
from .api_enterkomputer import get_drones, get_soundcards, get_opticals

response = {}

# NOTE : untuk membantu dalam memahami tujuan dari suatu fungsi (def)
# Silahkan jelaskan menggunakan bahasa kalian masing-masing, di bagian atas
# sebelum fungsi tersebut.

# ======================================================================== #
# User Func

# index func : jika user sudah login pada session, maka akan redirect ke url lab-9/profile
# yang kemudian akan memanggil fungsi profile.
# Namun, jika user belum login, maka akan ditampilkan halaman login.html
def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else:
        html = 'lab_9/session/login.html'
        return render(request, html, response)

# set_data_for_session func: untuk mengatur response sesuai dengan data user pada session tersebut, seperti author
# mengambil data user_login pada session, dan lain-lain.
# kemudian mengecek apabila sudah terdapat drones, soundcards, atau opticals sebelumnya, untuk menghindari terjadinya
# error pada saat pertama kali login
def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['drones'] = get_drones().json()
    response['soundcards'] = get_soundcards().json()
    response['opticals'] = get_opticals().json()

    # print ("#drones = ", get_drones().json(), " - response = ", response['drones'])
    ## handling agar tidak error saat pertama kali login (session kosong)
    if 'drones' in request.session.keys():
        response['fav_drones'] = request.session['drones']
    # jika tidak ditambahkan else, cache akan tetap menyimpan data
    # sebelumnya yang ada pada response, sehingga data tidak up-to-date
    else:
        response['fav_drones'] = []

    if 'soundcards' in request.session.keys():
        response['fav_soundcards'] = request.session['soundcards']
    else:
        response['fav_soundcards'] = []
    
    if 'opticals' in request.session.keys():
        response['fav_opticals'] = request.session['opticals']
    else:
        response['fav_opticals'] = []

# profile func : melakukan pengecekan jika halaman profile langsung diakses, jika belum login maka akan diredirect ke
# url index, jika sudah login maka akan memanggil fungsi set_data_for_session untuk mengatur data. Kemudian
# render 'lab_9/session/profile.html'

def profile(request):
    print ("#==> profile")
    ## sol : bagaimana cara mencegah error, jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-9:index'))
    ## end of sol

    set_data_for_session(response, request)

    html = 'lab_9/session/profile.html'
    return render(request, html, response)

# ======================================================================== #

### Drones

# add_session_drones func : untuk menambahkan user's favorite drones
def add_session_drones(request, id):
    ssn_key = request.session.keys() #mengambil semua keys oada session
    if not 'drones' in ssn_key: #jika key drones belum ada, maka membuat key drones dengan objek id
        print ("# init drones ")
        request.session['drones'] = [id]
    else:
        drones = request.session['drones'] #jika key drones sudah ada
        print ("# existing drones => ", drones)
        if id not in drones: #jika drone dengan id tersebut belum ada pada drones
            print ('# add new item, then save to session')
            drones.append(id) #append drones
            request.session['drones'] = drones #update

    messages.success(request, "Berhasil tambah drone favorite")
    return HttpResponseRedirect(reverse('lab-9:profile')) #redirect ke url profile

# del_session_drones func : untuk menghapus drone dari list favorite drones
def del_session_drones(request, id):
    print ("# DEL drones")
    drones = request.session['drones'] # mengambil list drones
    print ("before = ", drones)
    drones.remove(id) #untuk remove id tertentu dari list
    request.session['drones'] = drones
    print ("after = ", drones)

    messages.error(request, "Berhasil hapus dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile')) #redirect ke url profile

# clear_session_drones func : untuk reset seluruh drones pada favorite drones
def clear_session_drones(request):
    print ("# CLEAR session drones")
    print ("before 1 = ", request.session['drones'])
    del request.session['drones'] #delete list favorite drones

    messages.error(request, "Berhasil reset favorite drones")
    return HttpResponseRedirect(reverse('lab-9:profile')) #redirect ke url profile

# ======================================================================== #
# COOKIES

# cookie_login func : fungsi login pada cookie. Jika user sudah login maka redirect ke url cookie_profile. Jika belum maka akan render login.html
def cookie_login(request):
    print ("#==> masuk login")
    if is_login(request):
        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
    else:
        html = 'lab_9/cookie/login.html'
        return render(request, html, response)

# cookie_auth_login : untuk autentikasi login dari user cookie
def cookie_auth_login(request):
    print ("# Auth login")
    if request.method == "POST": #form pada login.html akan menjalankan post saat diklik login
        user_login = request.POST['username'] #mengambil username pada form
        user_password = request.POST['password'] #mengambil password pada form

        if my_cookie_auth(user_login, user_password): #mengecek apakah username dan password sama dengan cookie user
            print ("#SET cookies")
            res = HttpResponseRedirect(reverse('lab-9:cookie_login')) # redirect ke url cookie_login

            res.set_cookie('user_login', user_login) #set user_login pada cookie
            res.set_cookie('user_password', user_password) #set user_password pada cookie

            return res
        else:
            msg = "Username atau Password Salah"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('lab-9:cookie_login')) #redirect ke url cookie_login
    else:
        return HttpResponseRedirect(reverse('lab-9:cookie_login')) #redirect ke url cookie_login

# cookie_profile func : untuk menampilkan halaman profile cookie
def cookie_profile(request):
    print ("# cookie profile ")
    # method ini untuk mencegah error ketika akses URL secara langsung
    if not is_login(request):
        print ("belum login")
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        # print ("cookies => ", request.COOKIES)
        in_uname = request.COOKIES['user_login']
        in_pwd= request.COOKIES['user_password']

        # jika cookie diset secara manual (usaha hacking), distop dengan cara berikut
        # agar bisa masuk kembali, maka hapus secara manual cookies yang sudah diset
        if my_cookie_auth(in_uname, in_pwd):
            html = "lab_9/cookie/profile.html"
            res =  render(request, html, response)
            return res
        else:
            print ("#login dulu")
            msg = "Kamu tidak punya akses :P "
            messages.error(request, msg)
            html = "lab_9/cookie/login.html"
            return render(request, html, response)

# untuk menghapus / reset cookie
def cookie_clear(request):
    res = HttpResponseRedirect('/lab-9/cookie/login')
    res.delete_cookie('lang')
    res.delete_cookie('user_login')

    msg = "Anda berhasil logout. Cookies direset"
    messages.info(request, msg)
    return res

# mengecek apakah username dan password yang diinput sama dengan my username dan password
def my_cookie_auth(in_uname, in_pwd):
    my_uname = "redvelvet" #SILAHKAN ganti dengan USERNAME yang kalian inginkan
    my_pwd = "tiramisu" #SILAHKAN ganti dengan PASSWORD yang kalian inginkan
    return in_uname == my_uname and in_pwd == my_pwd

# mengetahui apakah sudah login ke cookie atau belum
def is_login(request):
    return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES




# ======================================================================== #

### Soundcards
def add_session_soundcards(request, id):
    ssn_key = request.session.keys()
    if not 'soundcards' in ssn_key:
        print ("# init soundcards ")
        request.session['soundcards'] = [id]
    else:
        soundcards = request.session['soundcards']
        print ("# existing soundcards => ", soundcards)
        if id not in soundcards:
            print ('# add new item, then save to session')
            soundcards.append(id)
            request.session['soundcards'] = soundcards

    messages.success(request, "Berhasil tambah soundcards favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_soundcards(request, id):
    print ("# DEL drones")
    soundcards = request.session['soundcards']
    print ("before = ", soundcards)
    soundcards.remove(id) #untuk remove id tertentu dari list
    request.session['soundcards'] = soundcards
    print ("after = ", soundcards)

    messages.error(request, "Berhasil hapus dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_soundcards(request):
    ssn_key = request.session.keys()
    if not 'soundcards' in ssn_key:
        messages.error(request, "Tidak ada favorite soundcards")
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else: 
        print ("# CLEAR session soundcards")
        print ("before 1 = ", request.session['soundcards'])
        del request.session['soundcards']

        messages.error(request, "Berhasil reset favorite soundcards")
        return HttpResponseRedirect(reverse('lab-9:profile'))

# ======================================================================== #

### Opticals
def add_session_opticals(request, id):
    ssn_key = request.session.keys()
    if not 'opticals' in ssn_key:
        print ("# init opticals ")
        request.session['opticals'] = [id]
    else:
        opticals = request.session['opticals']
        print ("# existing opticals => ", opticals)
        if id not in opticals:
            print ('# add new item, then save to session')
            opticals.append(id)
            request.session['opticals'] = opticals

    messages.success(request, "Berhasil tambah opticals favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_opticals(request, id):
    print ("# DEL opticals")
    opticals = request.session['opticals']
    print ("before = ", opticals)
    opticals.remove(id) #untuk remove id tertentu dari list
    request.session['opticals'] = opticals
    print ("after = ", opticals)

    messages.error(request, "Berhasil hapus dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_opticals(request):
    ssn_key = request.session.keys()
    if not 'opticals' in ssn_key:
        messages.error(request, "Tidak ada favorite opticals")
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else:    
        print ("# CLEAR session opticals")
        print ("before 1 = ", request.session['opticals'])
        del request.session['opticals']

        messages.error(request, "Berhasil reset favorite opticals")
        return HttpResponseRedirect(reverse('lab-9:profile'))

# ======================================================================== #